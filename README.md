![pipeline](https://codebase.helmholtz.cloud/hzb/epics/base/ubuntu_20_04/badges/docker_image_branch/pipeline.svg)
# Epics base
- Version of EPICS : `7.0.7`
- Version of Ubuntu : `20.04`


This image puts EPICS base at /opt/epics/base
This image tries to keep the image relatively by deleting EPICS source and keeping the cache clean.

## Pipeline

Pipeline can be distinguished into two main branches: build and test. 

The build can be triggered either by a new tag or merge to the main branch. The tests validate the image and store the outcome in a .log file as an artifact. If the image with tag latest doesn't exist it will be build and pushed to the registry. 

1. Use kaniko to build the image. ([kaniko](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html))
2. Do just a basic test to validate the image (manifest).

## Building

```bash
docker login registry.hzdr.de

docker build -t registry.hzdr.de/hzb/epics/base/ubuntu_20_04:1.1.0 .

docker push registry.hzdr.de/hzb/epics/base/ubuntu_20_04
``````
